<?php

/**
 * @file
 * This file to provide documentation for hooks.
 */

/**
 * Hook to alter the Authorize.Net recurring settings pane.
 *
 * By default user has to enter recurring interval for payment. We can disable
 * recurring settings to user and set recurring settings programmatically
 * accordingly to product or order properties. Use this hook and return
 * recurring settings as:
 *
 * return $recurring = array('period' => 'months','step' => 12);
 *
 * where allowed values for 'period' is 'month' or 'days' and for 'step' is
 * from 7 to 365  for 'days' and from 1 to 12 for 'years'.
 */
function hook_commerce_authorize_arb_commerce_set_recurring($order) {

}

/**
 * This hook will be invoked on each silent post from Authorize.Net.
 */
function hook_commerce_authorize_arb_silentpost($post) {

}
