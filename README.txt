CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Testing

INTRODUCTION
------------

Authorize.Net payment gateway integration for ARB (Automated Recurring Billing)
using latest sdk available for PHP and by means of recurring payment method
'commerce_authorize_arb'.

This module has integration with commerce (drupal.org/project/commerce) it
stores subscription information in $order->data and saves silentpost requests
as a commerce_payment transactions attached to the order.

REQUIREMENTS
------------

This module requires the following modules:

 * Commerce
 * Commerce Payment
 * Commerce UI
 * Commerce Payment UI
 * Commerce Authnet

INSTALLATION
------------

Please read INSTALL.txt file on how to install the module.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

    - Administer Authorize.net ARB settings

     Users in roles with the "Administer Authorize.net ARB settings" permission
     will be able to change the Authorize.net ARB credential settings.

 * Configure Authorize.net ARB settings in Administration » Configuration »
   Authorize.Net ARB Settings.

MAINTAINERS
-----------

Current maintainers:
 * Manoj Kumar - https://www.drupal.org/u/manojapare
 * Miranda Jose - https://www.drupal.org/u/miranda-jose
 * Aditya Anurag - https://www.drupal.org/u/aditya_anurag
 * Rakesh James - https://www.drupal.org/u/rakeshgectcr

TESTING
-------

Authorize.Net will send a silentpost request after you send a subscription
only after a day (If it is been configured). This makes debugging and testing
much harder. To make it easier the module emulate the request by means of a
special url: 'http://<yoursite>/commerce-authorize-arb-silentpost-test'. When
requesting it (actually with no post data) the module will emulate a silentpost
request similar to ARB silentpost. You may see the post data in
commerce_authet_arb_get_sample_silentpost_post() function. If you want to change
the data you can edit the function or pass parameters you want to change by GET
parameters, like:
'http://<yoursite>/commerce-authorize-arb-silentpost-test?x_cust_id=1&x_amount=1'
