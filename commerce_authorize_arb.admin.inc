<?php

/**
 * @file
 * Commerce Authorize.net ARB admin setting file.
 */

/**
 * A module settings form.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state.
 *
 * @return mixed
 *   System config form.
 */
function commerce_authorize_arb_settings_form($form, $form_state) {
  $form = commerce_authorize_arb_settings_pane();
  return system_settings_form($form);
}

/**
 * Commerce Authorize.net ARB settings form.
 *
 * @param array $settings
 *   Payment method settings.
 * @param bool $form_disabled
 *   Boolean form disabled.
 *
 * @return array
 *   Settings form array.
 */
function commerce_authorize_arb_settings_pane($settings = NULL, $form_disabled = FALSE) {
  if (empty($settings)) {
    $settings = commerce_authorize_arb_settings();
  }
  $disabled = ($form_disabled === FALSE) ? FALSE : TRUE;
  $form = array();

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authorize.Net payment settings'),
  );

  $settings_login = isset($settings['login']) ? $settings['login'] : '';
  $settings_tran_key = isset($settings['tran_key']) ? $settings['tran_key'] : '';
  $settings_sandbox = isset($settings['sandbox']) ? $settings['sandbox'] : FALSE;
  $settings_md5_hash = isset($settings['md5_hash']) ? $settings['md5_hash'] : '';
  $settings_watchdog_all = isset($settings['watchdog_all']) ? $settings['watchdog_all'] : FALSE;

  $form['settings']['commerce_authorize_arb_login_id'] = array(
    '#title' => t('API Login ID'),
    '#type' => 'textfield',
    '#disabled' => $disabled,
    '#default_value' => variable_get('commerce_authorize_arb_login_id', $settings_login),
    '#description' => t('Authorize.Net API Login ID.
      After logging in to Authorize.Net, can be found on Account tab and click the API Login ID and Transaction Key link to find your API Login ID.'),
  );

  $form['settings']['commerce_authorize_arb_tran_key'] = array(
    '#title' => t('Transaction Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_authorize_arb_tran_key', $settings_tran_key),
    '#description' => t('Transaction Key, which can be obtained from the same screen as of API Login ID.'),
  );

  $form['settings']['commerce_authorize_arb_sandbox'] = array(
    '#title' => t('Sandbox Mode'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('commerce_authorize_arb_sandbox', $settings_sandbox),
    '#description' => t('Sandbox mode interact with Authorize.Net development test server.'),
  );

  $form['settings']['commerce_authorize_arb_md5_hash'] = array(
    '#title' => t('MD5 Hash Secret Value'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_authorize_arb_md5_hash', $settings_md5_hash),
    '#description' => t("The MD5 Hash Value."),
  );

  $form['settings']['commerce_authorize_arb_debugging_watchdog_all'] = array(
    '#title' => t('Log all requests, responses and silentpost requests'),
    '#type' => 'checkbox',
    '#default_value' => $settings_watchdog_all,
  );

  return $form;
}
