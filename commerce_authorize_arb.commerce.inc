<?php

/**
 * @file
 * Integration with a commerce framework.
 */

/**
 * Authorize.net ARB payment method settings form.
 *
 * @param array $settings
 *   Payment method settings.
 *
 * @return array
 *   Settings form.
 */
function commerce_authorize_arb_commerce_settings_pane($settings = NULL) {
  $arb_settings = NULL;
  if (!empty($settings)) {
    $arb_settings = array(
      'login' => $settings['settings']['commerce_authorize_arb_login_id'],
      'tran_key' => $settings['settings']['commerce_authorize_arb_tran_key'],
      'sandbox' => $settings['settings']['commerce_authorize_arb_sandbox'],
      'md5_hash' => $settings['settings']['commerce_authorize_arb_md5_hash'],
      'watchdog_all' => $settings['settings']['commerce_authorize_arb_debugging_watchdog_all'],
    );
  }
  module_load_include('inc', 'commerce_authorize_arb', 'commerce_authorize_arb.admin');
  $form = commerce_authorize_arb_settings_pane($arb_settings, TRUE);

  drupal_set_message(t('You may change ARB payment settings on !page page',
    array('!page' => l(t('ARB settings'), 'admin/config/commerce-authorize-arb'))), 'warning');

  return $form;
}

/**
 * Payment method callback: submit form.
 *
 * @param array $payment_method
 *   Payment method.
 * @param mixed $pane_values
 *   Pane values.
 * @param mixed $checkout_pane
 *   Checkout pane.
 * @param array $order
 *   User order.
 *
 * @return array
 *   Payment pane form.
 */
function commerce_authorize_arb_payment_pane($payment_method, $pane_values, $checkout_pane, $order) {
  $form = array();
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  $credit_card = commerce_payment_credit_card_form(array('code' => ''));
  $form['credit_card'] = $credit_card['credit_card'];
  $recurring = array();
  // Allow hooks to alter the pane.
  $recurring = module_invoke_all('commerce_authorize_arb_commerce_set_recurring', $order);
  $form['recurring_options'] = commerce_authorize_arb_recurring_info_pane($recurring);
  $form['base_price'] = array(
    '#type' => 'hidden',
    '#value' => $order->commerce_order_total[LANGUAGE_NONE][0]['data']['components'][0]['price']['amount'],
  );
  return $form;
}

/**
 * Payment method callback: submit form validation.
 *
 * @param array $payment_method
 *   Payment method.
 * @param mixed $pane_form
 *   Pane form.
 * @param mixed $pane_values
 *   Pane values.
 * @param array $order
 *   User order.
 * @param array $form_parents
 *   Parent form.
 *
 * @return bool
 *   Valid or not.
 */
function commerce_authorize_arb_payment_pane_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {

  $valid = TRUE;

  // Validate the credit card fields.
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  $settings = array(
    'form_parents' => array_merge($form_parents, array('credit_card')),
  );
  if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
    $valid = FALSE;
  }

  // Validating recurring settings.
  if (commerce_authorize_arb_recurring_info_pane_validation($pane_values['recurring_options'], $form_parents) === FALSE) {
    $valid = FALSE;
  }

  if ($valid === FALSE) {
    return FALSE;
  }

  // Validate card processing.
  $charge = commerce_payment_order_balance($order);
  // If the line items was not added yet, set amout to $0.01.
  if (empty($charge['amount'])) {
    $charge['amount'] = '1';
  }
  if (!commerce_authorize_arb_verify_payment($charge, $pane_values['credit_card'])) {
    $prefix = implode('][', $form_parents) . '][';
    form_set_error($prefix, t("The card can't be authorized."));
    $valid = FALSE;
  }

  return $valid;
}

/**
 * Payment method callback: submit form submission.
 *
 * @param array $payment_method
 *   Payment method.
 * @param mixed $pane_form
 *   Pane form.
 * @param mixed $pane_values
 *   Pane values.
 * @param array $order
 *   User order.
 * @param array $charge
 *   Order charge.
 *
 * @return mixed
 *   Subscription.
 */
function commerce_authorize_arb_payment_pane_submit($payment_method, $pane_form, $pane_values, &$order, $charge) {
  if (isset($pane_values['base_price'])) {
    $charge['amount'] = $pane_values['base_price'];
  }
  return commerce_authorize_arb_create_subscription($pane_values['recurring_options'], $charge, $pane_values['credit_card'],
    array(), $order);
}

/**
 * Form pane to set recurring settings.
 *
 * @param array $recurring
 *   Recurring values.
 *
 * @return array
 *   Form.
 */
function commerce_authorize_arb_recurring_info_pane($recurring = array()) {

  $form = array();

  $form['commerce_authorize_arb_period'] = array(
    '#type' => 'select',
    '#title' => t('Period'),
    '#options' => array(
      'days' => t('Daily'),
      'months' => t('Monthly'),
    ),
    '#default_value' => 'none',
  );
  if (isset($recurring['period']) && in_array($recurring['period'], array_flip($form['commerce_authorize_arb_period']['#options']))) {
    $form['commerce_authorize_arb_period']['#value'] = $recurring['period'];
    $form['commerce_authorize_arb_period']['#access'] = FALSE;
  }

  $form['commerce_authorize_arb_interval_step'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#title' => t('Interval'),
    '#description' => t('Allowed values for monthly is 1 to 12 and for daily is 7 to 365.'),
  );
  if (isset($recurring['step'])) {
    $form['commerce_authorize_arb_interval_step']['#value'] = $recurring['step'];
    $form['commerce_authorize_arb_interval_step']['#access'] = FALSE;
  }

  return $form;
}

/**
 * Validation callback for recurring settings.
 *
 * @param array $pane_values
 *   Pane form values.
 * @param array $form_parents
 *   Parent form.
 *
 * @return bool
 *   Valid or not.
 */
function commerce_authorize_arb_recurring_info_pane_validation($pane_values, $form_parents = array()) {
  $prefix = implode('][', $form_parents) . '][recurring_options][';

  $valid = TRUE;

  // If period is set, interval is requered.
  if ($pane_values['commerce_authorize_arb_period'] == 'days' || $pane_values['commerce_authorize_arb_period'] == 'months') {
    // Perform validation only if allowed requring period passed.
    if (empty($pane_values['commerce_authorize_arb_interval_step'])) {
      $valid = FALSE;
      form_set_error($prefix . 'commerce_authorize_arb_interval_step', t('Recurring interval is required if the Recurring period is selected.'));
    }

    if ($pane_values['commerce_authorize_arb_period'] == 'days' &&
       ($pane_values['commerce_authorize_arb_interval_step'] < 7 || $pane_values['commerce_authorize_arb_interval_step'] > 365)) {
      $valid = FALSE;
      form_set_error($prefix . 'commerce_authorize_arb_interval_step', t('Recurring interval value must be between 7 and 365 for daily.'));
    }

    if ($pane_values['commerce_authorize_arb_period'] == 'months' &&
       ($pane_values['commerce_authorize_arb_interval_step'] < 1 || $pane_values['commerce_authorize_arb_interval_step'] > 12)) {
      $valid = FALSE;
      form_set_error($prefix . 'commerce_authorize_arb_interval_step', t('Recurring interval value must be between 1 and 12 for monthly.'));
    }
  }

  return $valid;
}

/**
 * Validate is card can be processed by authorize.net payment gateway.
 *
 * @param array $charge
 *   Order charge.
 * @param array $card
 *   Credit card.
 * @param array $response
 *   Payment gateway response.
 *
 * @return bool
 *   Payment success or failure.
 */
function commerce_authorize_arb_verify_payment($charge, $card, &$response = array()) {
  if ($charge['currency_code'] != 'USD') {
    drupal_set_message(t('Only USD can be processed by AUthorize.Net ARB.'), 'error');
    return FALSE;
  }

  require_once commerce_authorize_arb_sdk_path() . '/vendor/autoload.php';
  $settings = commerce_authorize_arb_settings();

  $auth = new AuthorizeNetAIM($settings['login'], $settings['tran_key']);
  $auth->invoice_num = time();

  if ($settings['sandbox']) {
    $auth->setSandbox(TRUE);
  }
  else {
    $auth->setSandbox(FALSE);
  }

  $auth->amount = $charge['amount'] / 100;
  $auth->card_num = $card['number'];
  $auth->exp_date = $card['exp_year'] . '-' . $card['exp_month'];

  if ($settings['watchdog_all']) {
    $debug_auth = var_export($auth, TRUE);
    watchdog('commerce_authorize_arb', "CC verification request:<br> $debug_auth");
  }

  $response = $auth->authorizeOnly();
  if ($settings['watchdog_all']) {
    $debug_response = var_export($response, TRUE);
    watchdog('commerce_authorize_arb', "CC verification response:<br> $debug_response");
  }
  if ($response->approved) {
    $return = TRUE;
  }
  elseif ($response->error) {
    drupal_set_message($response->error_message, 'error');
    $return = FALSE;
  }
  elseif ($response->declined) {
    drupal_set_message(t('Credit card declined'), 'error');
    $return = FALSE;
  }

  if ($response->approved || $response->declined) {
    // Cancel authorize transaction by using void:
    // This transaction type can be used to cancel either an original
    // transaction that is not yet settled or an
    // entire order composed of more than one transaction. A Void prevents the
    // transaction or the order from being sent for settlement. A Void can be
    // submitted against any other transaction type.
    $void = new AuthorizeNetAIM($settings['login'], $settings['tran_key']);

    if ($settings['sandbox']) {
      $void->setSandbox(TRUE);
    }
    else {
      $void->setSandbox(FALSE);
    }

    $void->void($response->transaction_id);
  }

  return $return;
}
